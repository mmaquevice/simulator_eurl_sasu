library simulator_eurl_sasu;

import 'package:logging/logging.dart';
import 'package:intl/intl.dart';

part 'src/services/service_ir_is.dart';
part 'src/services/service_dividend.dart';
part 'src/services/service_util.dart';

part 'src/services/sasu/service_simulation_sasu.dart';
part 'src/services/sasu/service_salary.dart';

part 'src/models/dividend.dart';

part 'src/models/sasu/salary.dart';
part 'src/models/sasu/simulation_sasu.dart';
part 'src/models/sasu/charges/charges_sas.dart';
part 'src/models/sasu/charges/charge_sas.dart';
part 'src/models/sasu/charges/charge_sas_csg_crds.dart';
part 'src/models/sasu/charges/charge_sas_gmp.dart';
part 'src/models/sasu/charges/charge_sas_linear.dart';
part 'src/models/sasu/charges/charge_sas_thresholds.dart';
part 'src/models/sasu/charges/charge_sas_fixed.dart';

part 'src/services/eurl/service_remuneration.dart';
part 'src/services/eurl/service_simulation_eurl.dart';
part 'src/services/eurl/service_dividend_eurl.dart';

part 'src/models/eurl/charges/charges_eurl.dart';
part 'src/models/eurl/charges/charge_eurl.dart';
part 'src/models/eurl/charges/charge_eurl_allocation_familiale.dart';
part 'src/models/eurl/charges/charge_eurl_csg_crds.dart';
part 'src/models/eurl/charges/charge_eurl_formation_professionnelle.dart';
part 'src/models/eurl/charges/charge_eurl_invalidite_deces.dart';
part 'src/models/eurl/charges/charge_eurl_retraite_complementaire.dart';
part 'src/models/eurl/charges/charge_eurl_threshold.dart';
part 'src/models/eurl/charges/charge_eurl_rate.dart';

part 'src/models/eurl/remuneration.dart';
part 'src/models/eurl/simulation_eurl.dart';