part of simulator_eurl_sasu;


ServiceDividendEurl serviceDividendEurl = new ServiceDividendEurl();

class ServiceDividendEurl {

  Logger logger = new Logger('simulator.ServiceDividendEurl');

  Dividend computeDividend(double dividendGross, double capital) {
    if (dividendGross <= capital * 0.10) {
      double flatTax = 30;
      double charges = dividendGross * (flatTax / 100);
      double net = dividendGross - charges;
      return new Dividend(dividendGross, charges, net);
    }

    double flatTax = 30;
    double chargesFlatTax = capital * 0.10 * (flatTax / 100);
    double dividendsExceeding = dividendGross - capital * 0.10;
    double charges = chargesFlatTax + (dividendsExceeding * 0.468);
    double net = dividendGross - charges;
    return new Dividend(dividendGross, charges, net);
  }
}

