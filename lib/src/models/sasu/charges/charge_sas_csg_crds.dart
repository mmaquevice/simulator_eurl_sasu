part of simulator_eurl_sasu;


class ChargeSasCsgCrds extends ChargeSas {

  double coefficient;
  double threshold;
  bool isDeductible = false;

  ChargeSasCsgCrds(String name, double rateEmployee, double rateEmployer, double coefficient, double threshold, bool isDeductible) : super(name, rateEmployee, rateEmployer) {
    this.coefficient = coefficient;
    this.threshold = threshold;
    this.isDeductible = isDeductible;
  }

  double calculateBase(double gross, double chargesToAdd) {

    double partWithCoefficient = gross*coefficient;
    double partWithNoCoefficient = 0.0;

    if(gross >= threshold) {
      partWithCoefficient = threshold*coefficient;
      partWithNoCoefficient = (gross-threshold);
    }

    return partWithCoefficient + partWithNoCoefficient + chargesToAdd;
  }

  double compute(double amount, double taux) {
    return amount * taux/100;
  }

  @override
  double computeEmployer(double amount) {
    return compute(amount, rateEmployer);
  }

  @override
  double computeEmployee(double amount) {
    return compute(amount, rateEmployee);
  }
}