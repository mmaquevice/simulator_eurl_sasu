part of simulator_eurl_sasu;


class ChargeEurlRate extends ChargeEurl with Rate {

  ChargeEurlRate(String name, double rate) : super(name) {
    this.rate = rate;
  }
}

abstract class Rate implements Compute {

  double rate;

  double compute(double base) {
    return base * rate/100;
  }
}
