# simulator_eurl_sasu

Ce projet est un simulateur de charges pour les EURL / SASU. Il a été créé dans le but d'appréhender les mécanismes de calcul des différentes charges. Ces derniers évoluant rapidement, n'hésitez pas à proposer une pull-request si vous notez des corrections à apporter.

## Usage

Vous pouvez réaliser une simulation facilement :

        import 'package:simulator_eurl_sasu/simulator_eurl_sasu.dart';

        main() {
          print(serviceSimulationEurl.findBestSimulation(100000.00, 6000.00));
          print(serviceSimulationSasu.findBestSimulation(100000.00, 6000.00));
        }

