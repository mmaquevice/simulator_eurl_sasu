part of simulator_eurl_sasu;


class ChargeEurlAllocationFamiliale extends ChargeEurl with AllocationFamiliale {

  double rate = 0.00;

  ChargeEurlAllocationFamiliale(String name) : super(name);

  double compute(double base) {
    double charges = super.compute(base);
    rate = (base / charges) * 100;
    return charges;
  }
}

abstract class AllocationFamiliale implements Compute {

  double rate1 = 2.15;
  double threshold1 = 42478.00;
  double rate2 = 5.25;
  double threshold2 = 54062.00;

  double compute(double base) {
    double rate = 0.00;

    if(base <= threshold1) {
      rate = rate1;
    }
    if(base > threshold1 && base < threshold2) {
      rate = (((threshold2 - base)/(threshold2 - threshold1))*(rate2-rate1))+rate1;
    }
    if(base >= threshold2) {
      rate = rate2;
    }

    return (base * rate) / 100;
  }
}
