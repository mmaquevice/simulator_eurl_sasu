part of simulator_eurl_sasu;


ServiceIrIs serviceIrIs = new ServiceIrIs();

class ServiceIrIs {

  Logger logger = new Logger('simulator.ServiceIrIs');

  double computeIr(double amount) {

    List<ThresholdIr> thresholds = [new ThresholdIr(0.00, 9965.00, 0.00),
                            new ThresholdIr(9965.00, 27520.00, 14.00),
                            new ThresholdIr(27520.00, 73780.00, 30.00),
                            new ThresholdIr(73780.00, 156245.00, 41.00),
                            new ThresholdIr(156245.00, null, 45.00)];

    double ir = 0.00;

    for(ThresholdIr threshold in thresholds) {

      if(amount > threshold.threshold1 && threshold.threshold2 == null) {
        ir += ((amount - threshold.threshold1) * threshold.rate) / 100;

      } else if(amount > threshold.threshold1 && amount >= threshold.threshold2) {
        ir += ((threshold.threshold2 - threshold.threshold1) * threshold.rate) / 100;

      } else if(amount > threshold.threshold1 && amount < threshold.threshold2) {
        ir += ((amount - threshold.threshold1) * threshold.rate) / 100;
      }
    }

    return ir;
  }

  double computeIs(double profit) {

    double rate1 = 15.00;
    double threshold1 = 38120.00;
    double rate2 = 28.00;
    double threshold2 = 500000.00;
    double rate3 = 33.00;

    double charges = 0.00;
    if(profit <= threshold1) {
      charges = profit * rate1 / 100;
    }
    if(profit > threshold1 && profit <= threshold2) {
      charges = (threshold1 * rate1 / 100) + ((profit - threshold1) * rate2 / 100);
    }
    if(profit > threshold2) {
      charges = (threshold1 * rate1 / 100) + (threshold2 * rate2 / 100) + ((profit - threshold2) * rate3 / 100);
    }

    return charges;
  }
}

class ThresholdIr {

  double threshold1;
  double threshold2;
  double rate;

  ThresholdIr(this.threshold1, this.threshold2, this.rate);
}

