part of simulator_eurl_sasu;

class SimulationEurl {

  Logger logger = new Logger('simulator.SimulationEurl');

  double turnover;
  double runningCost;

  Remuneration remuneration;
  double taxIs;
  Dividend dividend;

  double taxIr;
  double net;

  double treasury;

  SimulationEurl(this.turnover, this.runningCost, this.remuneration,
      this.taxIs, this.dividend, this.taxIr, this.net, this.treasury);

  Map toPublicJson() {
    Map map = new Map();
    map["turnover"] = roundNumber(turnover);
    map["runningCost"] = roundNumber(runningCost);
    map["remuneration"] = remuneration.toPublicJson();
    map["taxIs"] = roundNumber(taxIs);
    map["dividend"] = dividend.toPublicJson();
    map["treasury"] = roundNumber(treasury);
    map["taxIr"] = roundNumber(taxIr);
    map["net"] = roundNumber(net);
    map["yield"] = roundNumber(net / (turnover - runningCost) * 100);

    return map;
  }

  String toString() {
    return """
      \n
      [SimulationEurl]
      turnover = $turnover
      runningCost = $runningCost
      remuneration = $remuneration
      taxIs = $taxIs
      dividend = $dividend
      taxIr = $taxIr
      net = $net
      treasury = $treasury
      yield = ${roundNumber(net / (turnover - runningCost) * 100)}
      """;
  }
}
