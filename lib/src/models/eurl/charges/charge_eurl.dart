part of simulator_eurl_sasu;

abstract class ChargeEurl implements Compute {

  Logger logger = new Logger('simulator.ChargeEurl');

  ChargeEurl(this.name);

  String name;

  String toString() {
    return """
      \n
      [ChargeEurl]
      name = $name
      """;
  }
}

abstract class Compute {
  double compute(double amount);
}

class ChargeEurlComputed {

  Logger logger = new Logger('simulator.ChargeEurlComputed');

  ChargeEurl charge;
  double chargeComputed;

  ChargeEurlComputed(this.charge, this.chargeComputed);

  Map toPublicJson() {
    Map map = new Map();
    map["name"] = charge.name;
    map["amount"] = roundNumber(chargeComputed);
    return map;
  }
  String toString() {
    return """
      \n
      [ChargeEurlComputed]
      charge = $charge
      chargeComputed = $chargeComputed
      """;
  }
}
