part of simulator_eurl_sasu;


ServiceSalary serviceSalary = new ServiceSalary();

class ServiceSalary {

  Logger logger = new Logger('simulator.ServiceSalary');

  double findGrossFromExtraGross(double turnover, double runningCost, double extraGross, {bool isEmployee: false}) {

    Salary salary = serviceUtil.findByDichotomy(1000, extraGross / 2, extraGross, extraGross, (newLimit) => computeSalary(newLimit, isEmployee: isEmployee), (computed) => (turnover - runningCost >= computed.extraGross), (computed) => computed.extraGross);
    return salary.gross;
  }

  double findGrossFromNet(double net) {

    Salary salary = serviceUtil.findByDichotomy(1000, net, net * 2, net, (newLimit) => computeSalary(newLimit), (_) => true, (computed) => computed.net);
    return salary.gross;
  }

  Salary computeSalary(double gross, {isEmployee: false}) {

    List<ChargeSas> chargesWithoutCsgCrds = charges.charges.where((ChargeSas charge) => !(charge is ChargeSasCsgCrds)).toList();
    List<ChargeSasCsgCrds> chargesCsgCrds = charges.charges.where((ChargeSas charge) => charge is ChargeSasCsgCrds).toList().cast<ChargeSasCsgCrds>();

    if(!isEmployee) {
      chargesWithoutCsgCrds = chargesWithoutCsgCrds.where((ChargeSas charge) => !charge.toApplyToEmployeeOnly).toList();
    }

    List<ChargeSasComputed> chargesComputed = chargesWithoutCsgCrds.map((ChargeSas charge) => new ChargeSasComputed(charge, charge.computeEmployee(gross), charge.computeEmployer(gross))).toList();

    return new Salary.computeFromCharges(gross, chargesComputed, chargesCsgCrds);
  }

  Salary computeMonthlySalary(double gross) {
    Salary annualSalary = computeSalary(gross * 12);
    Salary monthlySalary = annualSalary.convertAnnualToMonthly();
    return monthlySalary;
  }

}

